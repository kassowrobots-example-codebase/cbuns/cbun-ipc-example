/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <kr2_program_api/api_v1/bundles/arg_provider_xml.h>
#include <sys/types.h>
#include <sys/ipc.h>

#include "example/example_message_queue.h"
#include "example/logging.h"
#include "example/example_message_queue_structs.h"

using namespace kswx_mq_example;

// The class has to be registered, otherwise the robot user will not be able
// to create an instance of the device (ie. he would not be able to Apply it).
REGISTER_CLASS(kswx_mq_example::MessageQueueExample)

MessageQueueExample::MessageQueueExample(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api,
                                       const boost::property_tree::ptree &a_xml_bundle_node)
:    kr2_bundle_api::CustomDevice(a_api, a_xml_bundle_node) {}


int MessageQueueExample::onCreate()
{
    SUBSCRIBE(kr2_signal::HWReady, MessageQueueExample::onHWReady);

    return 0;
}

void MessageQueueExample::onHWReady(const kr2_signal::HWReady&)
{
    // automatically activate after reboot (if activated before)
    if (activation_tree_)
    {
        kr2_program_api::CmdResult<> result = activate(*activation_tree_);
        switch (result.result_)
        {
            case kr2_program_api::CmdResult<>::EXCEPTION:
                PUBLISH_EXCEPTION(result.code_, result.message_)
                break;
            case kr2_program_api::CmdResult<>::ERROR:
                PUBLISH_ERROR(result.code_, result.message_)
                break;
        }
    }
}

int MessageQueueExample::onDestroy() {
    // onDeactivate stops background thread, onDestroy 
    onDeactivate();

    return 0;
}

CBUN_PCALL MessageQueueExample::onActivate(const boost::property_tree::ptree &a_param_tree)
{
    // get power exponent parameter from activation parameters
    if (!processActivationParams(a_param_tree))
    {
        CBUN_PCALL_RET_ERROR(-1, "Error in activation parameters.");
    }

    // create MessageServer with THREAD_SLEEP_MS sleep time between cycles and unique path to get pseudo-unique queue key via ftok
    message_server_thread_ = std::make_unique<MessageServerThread>(THREAD_SLEEP_MS, "/home/nucky/kr2.root/CBuns/install/cbun_message_queue_example");

    // set power exponenet parameter in the message server thread and start the thread with a 100ms timeout
    message_server_thread_->setPowerExponent(power_exponent_);
    if (!message_server_thread_->start(100))
    {
        CBUN_PCALL_RET_ERROR(-1, "Failed to start thread.");
    }

    // get queue key from queue server (after the thread started with previous calls)
    key_t message_queue_id = message_server_thread_->getGeneratedQueueKey();
    if (message_queue_id == -1)
    {
        CBUN_PCALL_RET_ERROR(-1, "Failed to get message queue key.");
    }

    // send the queue key to sequences
    node_.put("extra_parameters.message_queue_id", message_queue_id);

    // initialize message client in the master thread
    if (!initializeMessageClient(message_queue_id)) 
    {
        CBUN_PCALL_RET_ERROR(-1, "Failed to connect to message queue.");
    }

    CBUN_PCALL_RET_OK;
}

bool MessageQueueExample::initializeMessageClient(key_t message_queue_id) {
    message_client_ = std::make_unique<kswx_message_queue::MessageClient<compute_req, compute_resp>>();

    // try connecting to the message queue, MessageServer will periodically accept new connections and 
    // process requests, with the specified period of approximately THREAD_SLEEP_MS. Thus we use 
    // THREAD_SLEEP_MS * 2 as the timeout.
    kswx_message_queue::result res = message_client_->connect(message_queue_id, THREAD_SLEEP_MS * 2);

    return res == kswx_message_queue::result::SUCCESS;
}

CBUN_PCALL MessageQueueExample::onDeactivate()
{
    // stop and join the MessageServer thread.
    if (message_server_thread_)
        message_server_thread_->stop(100);

    // MessageClient has to disconnect from queue to remove unprocessed requests
    // MessageServer will clean up the queue of unprocessed requests and replies on this channel
    message_client_ = NULL;

    CBUN_PCALL_RET_OK;
}

int MessageQueueExample::onBind()
{
    // get the queue key from node_
    key_t message_queue_id = node_.get<key_t>("extra_parameters.message_queue_id");

    // initialize message client in sequences
    if (!initializeMessageClient(message_queue_id))
    {
        return -1;
    }

    return 0;
}

int MessageQueueExample::onUnbind()
{
    // MessageClient has to disconnect from queue to remove unprocessed requests
    // MessageServer will clean up the queue of unprocessed requests and replies on this channel
    message_client_ = NULL;

    return 0;
}

double MessageQueueExample::unifiedSendRequest(compute_req_type req_type, double x, double y) {
    // check message client initialization
    if (!message_client_)
    {
        throw MessageQueueExampleException("Message queue client not initialized");
    }

    // create the request
    compute_req req;
    req.req_type = req_type;
    req.x = x;
    req.y = y;

    // send request to message queue
    LOG_INFO("[client] sending request")
    if (message_client_->send(req, 1) != kswx_message_queue::result::SUCCESS)
    {
        LOG_INFO("[client] failure to send");
        throw MessageQueueExampleException("[client] failure to send");
    }

    // wait to receive response. MessageServer will periodically process requests, 
    // with the specified period of approximately THREAD_SLEEP_MS. Thus we use 
    // THREAD_SLEEP_MS * 2 as the timeout.
    LOG_INFO("[client] waiting for response");
    compute_resp resp = compute_resp::constructFailure();
    if (message_client_->receive(resp, THREAD_SLEEP_MS * 2) != kswx_message_queue::result::SUCCESS)
    {
        LOG_INFO("[client] failure to receive");
        throw MessageQueueExampleException("[client] failure to receive");
    }

    // check whether the server successfully processed the request
    if (!resp.success)
    {
        LOG_INFO("[client] request not successful");
        throw MessageQueueExampleException("[client] request not successful");
    }
    else
    {
        LOG_INFO("[client] got response");
    }

    return resp.result;
}

CBUN_PCALL MessageQueueExample::calcAdd(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y)
{
    target = unifiedSendRequest(compute_req_type::ADD, x.d(), y.d());
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MessageQueueExample::calcSubtract(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y)
{
    target = unifiedSendRequest(compute_req_type::SUBTRACT, x.d(), y.d());
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MessageQueueExample::calcMultiply(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y)
{
    target = unifiedSendRequest(compute_req_type::MULTIPLY, x.d(), y.d());
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MessageQueueExample::calcDivide(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y)
{
    target = unifiedSendRequest(compute_req_type::DIVIDE, x.d(), y.d());
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MessageQueueExample::calcInverse(kr2_program_api::Number& target, const kr2_program_api::Number& x)
{
    target = unifiedSendRequest(compute_req_type::INVERSE, x.d(), 0);
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MessageQueueExample::calcPower(kr2_program_api::Number& target, const kr2_program_api::Number& x)
{
    target = unifiedSendRequest(compute_req_type::POWER, x.d(), 0);
    CBUN_PCALL_RET_OK;
}

kr2_program_api::Number MessageQueueExample::add(kr2_program_api::Number x, kr2_program_api::Number y)
{
    return unifiedSendRequest(compute_req_type::ADD, x.d(), y.d());
}

kr2_program_api::Number MessageQueueExample::subtract(kr2_program_api::Number x, kr2_program_api::Number y)
{
    return unifiedSendRequest(compute_req_type::SUBTRACT, x.d(), y.d());
}

kr2_program_api::Number MessageQueueExample::multiply(kr2_program_api::Number x, kr2_program_api::Number y)
{
    return unifiedSendRequest(compute_req_type::MULTIPLY, x.d(), y.d()); 
}

kr2_program_api::Number MessageQueueExample::divide(kr2_program_api::Number x, kr2_program_api::Number y)
{
    return unifiedSendRequest(compute_req_type::DIVIDE, x.d(), y.d());   
}

kr2_program_api::Number MessageQueueExample::inverse(kr2_program_api::Number x)
{
    return unifiedSendRequest(compute_req_type::INVERSE, x.d(), 0);   
}

kr2_program_api::Number MessageQueueExample::power(kr2_program_api::Number x)
{
    return unifiedSendRequest(compute_req_type::POWER, x.d(), 0); 
}

bool MessageQueueExample::processActivationParams(const boost::property_tree::ptree &tree)
{
    kr2_bundle_api::ArgProviderXml arg_provider(tree);

    const int EXPECTED_PARAMS = 1;
    if (arg_provider.getArgCount() != EXPECTED_PARAMS) {
        LOG_ERR("Unexpected param count: actual=" << arg_provider.getArgCount() << ", expected=" << EXPECTED_PARAMS);
        return false;
    }
    
    power_exponent_ = arg_provider.getInt(0);
    return true;
}