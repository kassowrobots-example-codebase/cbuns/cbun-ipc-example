/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "example/example_message_server_thread.h"
#include "example/logging.h"

using namespace kswx_mq_example;

MessageServerThread::MessageServerThread(int sleep_ms, const char* ftok_pathname) : queue_key_(-1), message_server_(), ftok_pathname_(ftok_pathname), PeriodicThread(sleep_ms) {}

void MessageServerThread::setPowerExponent(int power_exponent) {
    power_exponent_ = power_exponent;
}

void MessageServerThread::_initMethod()
{
    // during initialization, create the message server and initialize it with specified path and first available project_id between 0-255.
    message_server_ = std::make_unique<kswx_message_queue::MessageServer<compute_req, compute_resp>>();

    key_t queue_key;
    message_server_->initializeWithPath(ftok_pathname_, queue_key);
    queue_key_ = queue_key;
}

key_t MessageServerThread::getGeneratedQueueKey()
{
    return queue_key_;
}

void MessageServerThread::_cycleMethod()
{
    // periodically handle new clients and requests, approximately every sleep_ms milliseconds.
    handleNewClients();
    handleRequests();
}

void MessageServerThread::handleNewClients()
{
    kswx_message_queue::result res;
    
    do
    {
        long new_channel_id;
        // call message_server_->handleChannelRequest until all new clients are handled, rest is unnecessary; 10ms timeout
        // method will assign new clients channels for communication
        res = message_server_->handleChannelRequest(new_channel_id, 10);
        if (res == kswx_message_queue::result::SUCCESS)
        {
            if (new_channel_id > 0)
            {
                LOG_INFO("[server] new client: " << new_channel_id);
            }
            else
            {
                LOG_INFO("[server] lost client: " << new_channel_id);
            }
        }
        else if (res != kswx_message_queue::result::NO_MESSAGE)
        {
            LOG_ERR("[server] handle channel request error: " << kswx_message_queue::resultToString(res));
        }
    } while (res == kswx_message_queue::result::SUCCESS);
}

void MessageServerThread::handleRequests() 
{
    while (true)
    {
        // receive request
        compute_req client_req;
        long client_channel_id;
        kswx_message_queue::result res = message_server_->receiveRequest(client_req, client_channel_id);

        if (res == kswx_message_queue::result::SUCCESS)
        {
            LOG_INFO("[server] received request, sending response");

            // parse and evaluate request
            compute_resp server_resp = evaluateRequest(client_req);

            // send response to client, 10ms timeout
            res = message_server_->sendResponse(server_resp, client_channel_id, 10);
            if (res != kswx_message_queue::result::SUCCESS)
            {
                LOG_ERR("[server] transmit error: " << kswx_message_queue::resultToString(res));
            }
            else 
            {
                LOG_INFO("[server] transmit success");
            }
        }
        else
        {
            if (res != kswx_message_queue::result::NO_MESSAGE)
            {
                LOG_ERR("[server] receive error: " << kswx_message_queue::resultToString(res));
            }
            break;
        }
    }
}

compute_resp MessageServerThread::evaluateRequest(compute_req req)
{
    switch (req.req_type) {
        case compute_req_type::ADD: 
            return compute_resp::constructSuccess(req.x + req.y);
        case compute_req_type::SUBTRACT: 
            return compute_resp::constructSuccess(req.x - req.y);
        case compute_req_type::MULTIPLY: 
            return compute_resp::constructSuccess(req.x * req.y);
        case compute_req_type::DIVIDE: 
            return compute_resp::constructSuccess(req.x / req.y);
        case compute_req_type::INVERSE: 
            return compute_resp::constructSuccess(1 / req.x);
        case compute_req_type::POWER:
            {
                double retval = 1;
                for (int i = 0; i < power_exponent_; ++i)
                {
                    retval *= req.x;
                }
                return compute_resp::constructSuccess(retval);
            }
        default:
            return compute_resp::constructFailure();
    }
}