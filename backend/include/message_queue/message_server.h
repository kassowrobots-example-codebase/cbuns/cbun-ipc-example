/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_MESSAGE_SERVER_H
#define KR2_MESSAGE_SERVER_H

#include <message_queue/message_base.h>
#include <message_queue/message_structs.h>
#include <set>

namespace kswx_message_queue
{
    /// @brief class for server -> client communication, initialize queue, handle requests and send responses
    /// @tparam req_t type of client->server message (request)
    /// @tparam resp_t type of server->client message (response)
    template <typename req_t, typename resp_t>
    class MessageServer : MessageBase
    {
    public:
        MessageServer();
        virtual ~MessageServer();

        /// @brief Create a queue (exclusive), destroyed in destructor
        /// @param key identifies the queue, usually received from an ftok call
        /// @param force_connect if true and if queue with specified "key" already exists, "initialize" will remove it and create 
        /// a new one;   otherwise, if false and queue already exists, return INITIALIZATION_ERROR
        /// @return SUCCESS, ALREADY_INITIALIZED, INITIALIZATION_ERROR
        result initialize(key_t key, bool force_connect);

        /// @brief Create a queue (exclusive), destroyed in destructor. Uses ftok to generate key_t for the queue from path and 
        /// a project_id number - tries all numbers (0-255) and returns first generated key_t that successfully initializes the 
        /// queue in the generated_key parameter. 
        /// @param pathname path to be used in ftok call, to create a pseudo-unique key_t for queue
        /// @param generated_key used to return the first generated key that successfully initialized the queue or -1 if no 
        /// key successfully initialized the queue
        /// @return SUCCESS, ALREADY_INITIALIZED, INITIALIZATION_ERROR
        result initializeWithPath(const char* pathname, key_t& generated_key);

        /// @brief Handle a single channel request. Should be called periodically with frequency high enough to fulfil timeout 
        /// requirements in client. If at risk that many clients join during a single period, method should be called in a loop
        /// until all clients are handled.
        /// @param new_channel_id set to channel id of the new client channel (positive value), -1 * channel id of a closing client channel (negative value)
        /// or 0 if there is no client request since last call
        /// @param timeout_ms timeout in milliseconds or 0 for a blocking call
        /// @return SUCCESS on successfully handled channel request, NO_MESSAGE if there was no request, QUEUE_INVALID, COMMUNICATION_ERROR or TIMEOUT otherwise
        result handleChannelRequest(long& new_channel_id, long timeout_ms);

        /// @brief Non-blocking method to receive a request from a client. Should be called periodically with frequency high enough
        /// to fulfil timeout requirements in client. If at risk that many clients send request during a single period, method should
        /// be called in a loop until all clients are handled.
        /// @param request set to request from the client, not set if there is no request since last call
        /// @param channel_id set to the id of a client from whom the request came, not set if there is no request since last call
        /// @return SUCCESS on successfully handled channel request, NO_MESSAGE if there was no request, QUEUE_INVALID or COMMUNICATION_ERROR otherwise
        result receiveRequest(req_t& request, long& channel_id);

        /// @brief Send a response to a client. 
        /// @param response response to send
        /// @param channel_id identifies the client to which the response is sent, usually from receiveRequest
        /// @param timeout_ms timeout in milliseconds or 0 for a blocking call
        /// @return SUCCESS, QUEUE_INVALID, COMMUNICATION_ERROR, TIMEOUT or CHANNEL_NOT_OPEN
        result sendResponse(resp_t response, long channel_id, long timeout_ms);
        
    private:
        long next_channel_id;
        std::set<long> active_channels;
    };





    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    template <typename req_t, typename resp_t>
    inline MessageServer<req_t, resp_t>::MessageServer() : next_channel_id(MESSAGE_RESPONSE_INIT) {}

    template <typename req_t, typename resp_t>
    inline MessageServer<req_t, resp_t>::~MessageServer()
    {
        msgctl(queue_id, IPC_RMID, 0);
    }

    template <typename req_t, typename resp_t>
    inline result MessageServer<req_t, resp_t>::initialize(key_t key, bool force_connect)
    {
        if (queue_id != -1)
            return result::ALREADY_INITIALIZED;
            
        int res = msgget(key, 0600 | IPC_CREAT | IPC_EXCL);
        if (res == -1)
        {
            if (force_connect && errno == EEXIST)
            {
                res = msgget(key, 0600);
                if (res == -1)
                    return result::INITIALIZATION_ERROR;
                res = msgctl(res, IPC_RMID, 0);
                if (res == -1)
                    return result::INITIALIZATION_ERROR;
                res = msgget(key, 0600 | IPC_CREAT | IPC_EXCL);
                if (res == -1)
                    return result::INITIALIZATION_ERROR;
            }
            else
                return result::INITIALIZATION_ERROR;
        }
        queue_id = res;

        return result::SUCCESS;
    }
    
    template <typename req_t, typename resp_t>
    result MessageServer<req_t, resp_t>::initializeWithPath(const char* pathname, key_t& generated_key)
    {
        generated_key = -1;

        if (queue_id != -1)
            return result::ALREADY_INITIALIZED;

        kswx_message_queue::result res;
        for (int proj_id = 0; proj_id < 256; ++proj_id)
        {
            key_t ftok_queue_key = ftok(pathname, proj_id);
            if (proj_id != -1)
            {
                res = initialize(ftok_queue_key, false);
                if (res == kswx_message_queue::result::SUCCESS)
                {
                    generated_key = ftok_queue_key;
                    return result::SUCCESS;
                }
            }
        }

        return res;
    }
    
    template <typename req_t, typename resp_t>
    inline result MessageServer<req_t, resp_t>::handleChannelRequest(long &new_channel_id, long timeout_ms)
    {
        new_channel_id = 0;

        long deadline = getDeadline(timeout_ms);

        if (queue_id == -1)
            return result::QUEUE_INVALID;
        
        result res;

        channel_request_msgbuf req;
        res = receiveWithDeadline(req, 1, req.mtype);
        if (res != result::SUCCESS)
            return res;
            
        if (req.channel_id == -1)
        {
            channel_response_msgbuf resp(next_channel_id);
            res = sendWithDeadline(resp, deadline);
            if (res != result::SUCCESS)
                return res;

            new_channel_id = next_channel_id++;
            active_channels.insert(new_channel_id);
        }
        else
        {
            message_response_msgbuf<resp_t> resp_buf;
            while (receiveWithDeadline(resp_buf, 1, req.channel_id) == result::SUCCESS);
            active_channels.erase(req.channel_id);

            new_channel_id = -req.channel_id;
        }

        return result::SUCCESS;
    }
    
    template <typename req_t, typename resp_t>
    inline result MessageServer<req_t, resp_t>::receiveRequest(req_t &request, long& channel_id)
    {
        if (queue_id == -1)
            return result::QUEUE_INVALID;

        message_request_msgbuf<req_t> req_buf;
        result res = receiveWithDeadline(req_buf, 1, req_buf.mtype);
        if (res != result::SUCCESS)
            return res;
        
        request = req_buf.payload;
        channel_id = req_buf.channel_id;

        return result::SUCCESS;
    }
    
    template <typename req_t, typename resp_t>
    inline result MessageServer<req_t, resp_t>::sendResponse(resp_t response, long channel_id, long timeout_ms)
    {
        if (active_channels.find(channel_id) == active_channels.end())
            return result::CHANNEL_NOT_OPEN;

        long deadline = getDeadline(timeout_ms);

        if (queue_id == -1)
            return result::QUEUE_INVALID;
        
        message_response_msgbuf<resp_t> resp_buf(channel_id, response);
        result res = sendWithDeadline(resp_buf, deadline);
        if (res != result::SUCCESS)
            return res;

        return result::SUCCESS;
    }
}

#endif // KR2_MESSAGE_SERVER_H