/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_MESSAGE_STRUCTS_H
#define KR2_MESSAGE_STRUCTS_H

#include <cstddef>
#include <sys/types.h>

namespace kswx_message_queue
{
    enum message_type
    {
        /// @brief queue channel type for client requesting a new communication channel
        CHANNEL_REQUEST = 1,

        /// @brief queue channel type for server response granting a client a communication channel
        CHANNEL_RESPONSE = 2,

        /// @brief queue channel type for client->server message 
        MESSAGE_REQUEST = 3,

        /// @brief lowest queue channel type for server->client message
        MESSAGE_RESPONSE_INIT = 4
    };

    /// @brief request from a client - requesting a channel when channel_id is -1, requesting closing of specified channel otherwise
    struct channel_request_msgbuf
    {
        long mtype;
        long channel_id;

        channel_request_msgbuf();
        channel_request_msgbuf(long channel_id_);
        ssize_t size();
    };

    /// @brief server response sending client an available channel
    struct channel_response_msgbuf
    {
        long mtype;
        long channel_id;

        channel_response_msgbuf();
        channel_response_msgbuf(long channel_id_);
        ssize_t size();
    };

    /// @brief Message from client to server, request.
    /// @tparam req_type type of message
    template<typename req_type>
    struct message_request_msgbuf
    {
        long mtype;
        long channel_id;
        req_type payload;

        message_request_msgbuf();
        message_request_msgbuf(long channel_id_, req_type payload_);
        ssize_t size();
    };

    /// @brief Message from server to client, response.
    /// @tparam resp_type type of message
    template<typename resp_type>
    struct message_response_msgbuf
    {
        long mtype;
        resp_type payload;

        message_response_msgbuf();
        message_response_msgbuf(long channel_id_, resp_type payload_);
        ssize_t size();
    };





    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename req_type>
    inline message_request_msgbuf<req_type>::message_request_msgbuf() : mtype(MESSAGE_REQUEST), channel_id(-1) {}

    template <typename req_type>
    inline message_request_msgbuf<req_type>::message_request_msgbuf(long channel_id_, req_type payload_) : mtype(MESSAGE_REQUEST), channel_id(channel_id_), payload(payload_) {}

    template <typename resp_type>
    inline message_response_msgbuf<resp_type>::message_response_msgbuf() : mtype(-1) {}

    template <typename resp_type>
    inline message_response_msgbuf<resp_type>::message_response_msgbuf(long channel_id_, resp_type payload_) : mtype(channel_id_), payload(payload_) {}

    template <typename req_type>
    inline ssize_t message_request_msgbuf<req_type>::size()
    {
        return sizeof(message_request_msgbuf<req_type>) - sizeof(long);
    }

    template <typename resp_type>
    inline ssize_t message_response_msgbuf<resp_type>::size()
    {
        return sizeof(message_response_msgbuf<resp_type>) - sizeof(long);
    }
}

#endif // KR2_MESSAGE_STRUCTS_H