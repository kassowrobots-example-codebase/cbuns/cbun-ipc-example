/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_CBUN_MESSAGE_SERVER_THREAD
#define KR2_CBUN_MESSAGE_SERVER_THREAD

#include <memory>
#include <atomic>

#include "message_queue/message_server.h"
#include "periodic_thread/periodic_thread.h"
#include "example_message_queue_structs.h"

namespace kswx_mq_example {
    
    /// @brief thread for handling requests coming via the message queue
    class MessageServerThread : public kswx_periodic_thread::PeriodicThread {
    public:
        MessageServerThread(int sleep_ms, const char* ftok_pathname);
        virtual ~MessageServerThread() = default;

        void setPowerExponent(int power_exponent);

        /// @brief returns key to the intialized queue or -1 if no queue initialized yet
        key_t getGeneratedQueueKey();
        
    protected:
        /// @brief method that is called once, during the initialization of the thread
        virtual void _initMethod() override;

        /// @brief method that is called once per cycle
        virtual void _cycleMethod() override;

    private:
        void handleNewClients();
        void handleRequests();
        compute_resp evaluateRequest(compute_req req);

        std::unique_ptr<kswx_message_queue::MessageServer<compute_req, compute_resp>> message_server_;
        std::atomic<int> power_exponent_;
        std::atomic<key_t> queue_key_;
        const char* ftok_pathname_;
    };

} // namespace kswx_mq_example

#endif // KR2_CBUN_MESSAGE_SERVER_THREAD