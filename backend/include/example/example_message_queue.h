/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_CBUN_MESSAGE_QUEUE_EXAMPLE
#define KR2_CBUN_MESSAGE_QUEUE_EXAMPLE

#include <memory>

#include <kr2_program_api/api_v1/bundles/custom_device.h>
#include "message_queue/message_client.h"
#include "example_message_server_thread.h"
#include "example_message_queue_structs.h"

#define THREAD_SLEEP_MS 100


namespace kswx_mq_example {

    class MessageQueueExampleException : public std::exception {
    public:
        inline MessageQueueExampleException(std::string msg)
        {
            msg_ = msg;
        }
        
        inline const char* what() const throw()
        {
            return msg_.c_str();
        }
        
    protected:
        std::string msg_;
    };
    
    class MessageQueueExample : public kr2_bundle_api::CustomDevice
    {
    public:

        MessageQueueExample(boost::shared_ptr<kr2_program_api::ProgramInterface> api, const boost::property_tree::ptree &xml_bundle_node);
        virtual ~MessageQueueExample() = default;




        // fixed bundle class methods

        virtual int onCreate();

        virtual int onDestroy();

        virtual int onBind();

        virtual int onUnbind();

        void onHWReady(const kr2_signal::HWReady&);


        // custom methods

        virtual CBUN_PCALL calcAdd(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y);

        virtual CBUN_PCALL calcSubtract(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y);

        virtual CBUN_PCALL calcMultiply(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y);

        virtual CBUN_PCALL calcDivide(kr2_program_api::Number& target, const kr2_program_api::Number& x, const kr2_program_api::Number& y);

        virtual CBUN_PCALL calcInverse(kr2_program_api::Number& target, const kr2_program_api::Number& x);

        virtual CBUN_PCALL calcPower(kr2_program_api::Number& target, const kr2_program_api::Number& x);

        // custom functions

        virtual kr2_program_api::Number add(kr2_program_api::Number x, kr2_program_api::Number y);

        virtual kr2_program_api::Number subtract(kr2_program_api::Number x, kr2_program_api::Number y);

        virtual kr2_program_api::Number multiply(kr2_program_api::Number x, kr2_program_api::Number y);

        virtual kr2_program_api::Number divide(kr2_program_api::Number x, kr2_program_api::Number y);

        virtual kr2_program_api::Number inverse(kr2_program_api::Number x);

        virtual kr2_program_api::Number power(kr2_program_api::Number x);
        
    protected:
    
        virtual CBUN_PCALL onActivate(const boost::property_tree::ptree &param_tree);

        virtual CBUN_PCALL onDeactivate();

        virtual inline CBUN_PCALL onMount(const boost::property_tree::ptree &param_tree) { CBUN_PCALL_RET_OK }

        virtual inline CBUN_PCALL onUnmount() { CBUN_PCALL_RET_OK }

    private:
        /// @brief Parse activation parameters.
        bool processActivationParams(const boost::property_tree::ptree &tree);

        /// @brief Initialize the message client.
        /// @param message_queue_id queue key that identifies the queue to connect to
        /// @return true on success, false otherwise.
        bool initializeMessageClient(key_t message_queue_id);

        /// @brief send request with specified parameters via the message queue and return reply.
        /// Throw exception on send/receive failure, if success == false in reply or if message client is null = not initialized.
        double unifiedSendRequest(compute_req_type req_type, double x, double y);

        int power_exponent_;
        std::unique_ptr<kswx_message_queue::MessageClient<compute_req, compute_resp>> message_client_;
        std::unique_ptr<MessageServerThread> message_server_thread_;
    };

} // namespace kswx_mq_example

#endif // KR2_CBUN_MESSAGE_QUEUE_EXAMPLE