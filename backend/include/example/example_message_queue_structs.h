/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, KR Soft s.r.o.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_CBUN_MESSAGE_QUEUE_EXAMPLE_STRUCTS
#define KR2_CBUN_MESSAGE_QUEUE_EXAMPLE_STRUCTS

namespace kswx_mq_example {
    
    /// @brief type of computation requested
    enum class compute_req_type {
        ADD, SUBTRACT, MULTIPLY, DIVIDE, INVERSE, POWER
    };

    /// @brief struct contatining the request (client -> server)
    struct compute_req {
        compute_req_type req_type;
        double x;
        double y;
    };

    /// @brief struct containing the response (server -> client)
    struct compute_resp {
        inline compute_resp() : success(false), result(0) {}

        inline compute_resp(bool success_, double result_) : success(success_), result(result_) {}

        /// @brief Construct a failure response.
        inline static compute_resp constructFailure() { return compute_resp(false, 0); }

        /// @brief Construct a success response with value.
        inline static compute_resp constructSuccess(double retval) { return compute_resp(true, retval); }

        bool success;
        double result;
    };

} // namespace kswx_mq_example

#endif // KR2_CBUN_MESSAGE_QUEUE_EXAMPLE_STRUCTS